
#include <stdlib.h>
#include <stdint.h>

typedef enum tlv_result {
    SUCCESS,
    ILLEGAL_LENGTH,
    DUPLICATED_FIELD,
    BUFFER_OVERRUN
} tlv_result;

// Allow variable length. See the bitflags field below.
#define TLV_VARIABLE_LENGTH 0x1

// Structure that specifies the accepted formats of the data, and store the data
// itself.
//
// tag:     The tag of the field
// length:  The length of the field, or -1 if you want it to be inserted. 
//          WARNING: DO NOT USE length=-1 IN CONJUNCTION WITH data!=NULL!!!
// bitflags: Flags that specify whether the discovered length can be less 
//          than the specified length.
// data:    The eventual spot where the data is copied. If NULL, the pointer 
//          will be modified to point at the data instead. 
//          WARNING: NO GUARANTEES ABOUT ALIGNMENT CAN BE MADE. ADDITIONALLY,
//          THE POINTER *WILL* DANGLE ONCE THE BUFFER IS DESTROYED!
struct tlvinfo {
    uint32_t tag;
    uint32_t length;
    uint32_t bitflags;
    void * data;
};

// Parses the buffer into the info array. 
// In general, this function should not segfault or write outside the prospected buffers.
//
// Args:
//      buffer: target buffer
//      info: array of possible info structures
tlv_result tlv_read(char * buffer, unsigned int buffer_length, struct tlvinfo * info, unsigned int info_length) ;

// Returns the minimum necessary size of a buffer when writing the info into it
uint32_t tlv_size(struct tlvinfo * info, unsigned int info_length) ;

// Writes the info into a buffer. In contrast to the reading function, which might read from 
// an untrusted source, this function assumes the information is safe, and thus has a more
// "lol good luck" attitude
tlv_result tlv_write(char * buffer, struct tlvinfo * info, unsigned int info_length) ;
