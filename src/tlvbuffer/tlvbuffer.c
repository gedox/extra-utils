#include "tlvbuffer.h"
#include "test.h"
#include <stdio.h>
#include <string.h>

// Flag to check whether we already wrote data into a field to prevent double writes and stuff
#define TLV_HIT 0x80000000

// Parses the buffer into the info array. 
// In general, this function should not segfault or write outside the prospected buffers.
//
// Args:
//      buffer: target buffer
//      info: array of possible info structures
tlv_result tlv_read(char * buffer, unsigned int buffer_length, struct tlvinfo * info, unsigned int info_length) {
    char * buffer_end = buffer + buffer_length;

    for (int i = 0; i < info_length; i++) {
        info[i].bitflags &= ~TLV_HIT;
    }

    while (buffer < buffer_end) {
        unsigned int tag, length;
        
        // Buffer might be misaligned, so tag = *(int*) buffer is UB
        memcpy(&tag, buffer, sizeof(tag));
        buffer += sizeof(tag);
        memcpy(&length, buffer, sizeof(length));
        buffer += sizeof(length);

        // Iterate over the possible formats to see if one matches
        for(int i = 0; i < info_length; i++) {
            // Wrong tag
            if (info[i].tag != tag) {
                continue;
            }

            struct tlvinfo * hit = &info[i];

            // Already filled in the data
            if (hit->bitflags & TLV_HIT) {
                continue;
            }
            hit->bitflags |= TLV_HIT;

            // Checking for common attack vectors
            // On variable length (see the warnings!) we simply copy the length
            if (~hit->length == 0) {
                hit->length = length;
            }
            // Check if the lengths match when using fixed length
            else if (hit->length != length && !(hit->bitflags & TLV_VARIABLE_LENGTH)) {
                return ILLEGAL_LENGTH;
            }
            // Check if no field buffer overflow occurs when using variable lengths
            else if (hit->length < length) {
                return ILLEGAL_LENGTH;
            }

            // Check for argument buffer overrun
            if (buffer + hit->length > buffer_end) {
                return BUFFER_OVERRUN;
            }

            // Checks are done, now we can safely juggle with memory
            if (hit->data == NULL) {
                hit->data = buffer;
            } else {
                memcpy(hit->data, buffer, hit->length);
            }
            buffer += length;
            break;
        }
    }
    return SUCCESS;
}

// Returns the minimum necessary size of a buffer when writing the info into it
uint32_t tlv_size(struct tlvinfo * info, unsigned int info_length) {
    uint32_t length = 0;
    for(int i = 0; i < info_length; i++) {
        length += sizeof(info->length) + sizeof(info->tag) + info[i].length;
    }
    return length;
}

// Writes the info into a buffer. In contrast to the reading function, which might read from 
// an untrusted source, this function assumes the information is safe, and thus has a more
// "lol good luck" attitude
tlv_result tlv_write(char * buffer, struct tlvinfo * info, unsigned int info_length) {
    for(int i = 0; i < info_length; i++) {
        struct tlvinfo * current = &info[i];
        // Write the tag
        memcpy(buffer, &current->tag, sizeof(current->tag));
        buffer += sizeof(current->tag);
        // Write the length
        memcpy(buffer, &current->length, sizeof(current->length));
        buffer += sizeof(current->length);
        // Write the data
        memcpy(buffer, current->data, current->length);
        buffer += current->length;
    }
    return SUCCESS;
}


TEST(tlvbuffer) {
    int t = 256;
    struct tlvinfo serialize [3] = {{0, 6, 0, "Hello"}, {1, sizeof(t), 0, &t}, {0, 10, 0, "My number"}};
    int necessary_length = tlv_size(serialize, 3);
    char * buffer = malloc(necessary_length);
    tlv_write(buffer, serialize, 3);
    t = 0;
    struct tlvinfo deserialize [3] = {{0, -1, 0, NULL}, {1, sizeof(t), 0, &t}, {0, -1, 0, NULL}};
    tlv_result res;
    if (res = tlv_read(buffer, necessary_length, deserialize, 3)) {
        printf("Error code: %i]\n", res);
        ASSERT(0);
    }
    printf("%s, %s is %i\n", (char *) deserialize[0].data, (char *) deserialize[2].data, t);
    ASSERT(0);
}

TEST(other) {

}

