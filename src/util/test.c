#include "test.h"
struct test_chain * chain_start=0;
int err = 0;

TEST(test_success) {
    ASSERT(1);
}

TEST(test_failure) {
    ASSERT(0);
}

int main(){
    while (chain_start) {
        printf("Running %s... \n", chain_start->name);
        err = 0;
        chain_start->test();
        if (err) {
            printf("%s failed! \n", chain_start->name);
        }
        chain_start = chain_start->next;
    }
    exit(err);
}
