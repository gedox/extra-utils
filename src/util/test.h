#ifndef __TEST_H__
#define __TEST_H__
#include <stdio.h>
#include <stdlib.h>

#define TEST(xtest) struct test_chain chain_ ## xtest; \
    void xtest(void); \
    __attribute((constructor)) void premain_ ## xtest (void) {\
        chain_ ## xtest.test = xtest; \
        chain_ ## xtest.name = #xtest; \
        chain_ ## xtest.next = chain_start; \
        chain_start = &chain_ ## xtest; \
    }\
    void xtest(void) 
   

struct test_chain {
    void (*test) (void);
    char * name;
    struct test_chain * next;
};

extern struct test_chain * chain_start;

extern int err;

#define ASSERT(x) do { if (!(x)) {printf("Assertion failed in file %s line %i\n", __FILE__, __LINE__); err = (1);}} while (0)

#endif
